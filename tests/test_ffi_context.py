# import distutils._msvccompiler.MSVCCompiler
#
# class MSVCCompiler(distutils._msvccompiler.MSVCCompiler):
#     def spawn(self, cmd):
#         print(cmd)
#         super(MSVCCompiler, self).spawn(cmd)
#
# distutils._msvccompiler.MSVCCompiler = MSVCCompiler

import os
import sys
import pytest
import cfficloak
from cfficloak.ffi import FFI
from multiprocessing import Process
from os.path import join, abspath, dirname


def compile_test_lib():
    import cffi
    ffi = cffi.FFI()

    ffi.cdef('''
    int function(int arg);
    ''')

    ffi.set_source('_test_ffi_context', '''
    int function(int arg){
        return arg;
    }
    ''')
    return ffi.compile(verbose=True)


mod = compile_test_lib()

# Build the cffi module in a separate process to no taint the local modules space
# p = Process(target=compile_test_lib)
# p.start()
# p.join()

moddir = abspath(dirname(mod))
if moddir not in sys.path:
    sys.path.insert(0, moddir)

# noinspection PyUnresolvedReferences
import _test_ffi_context


def test_invalid_reentrant():
    # cloak defines as a context so should
    # fail if already in a context
    with pytest.raises(ValueError):
        with FFI(_test_ffi_context):
            _ = cfficloak.cloak(_test_ffi_context)


def test_no_context():
    ffi = FFI()
    assert 'cffi.api.FFI' in str(ffi)


def test_fn_in_context():
    def fn():
        assert 'CompiledFFI' in str(FFI())

    with FFI(_test_ffi_context) as ffi:
        assert 'CompiledFFI' in str(ffi)
        assert 'CompiledFFI' in str(FFI())
        fn()


