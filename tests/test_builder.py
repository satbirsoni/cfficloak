import os
import sys
import pytest
import tempfile

import cfficloak
from cfficloak.ffi import FFI
from cfficloak.functions import NullError
from distutils.dir_util import remove_tree
from cfficloak.builder import generate_function_skeletons, Path, import_file, parse_header
from multiprocessing import Process
from os.path import join, abspath, dirname


def compile_test_lib():
    import cffi
    ffi = cffi.FFI()

    include = '''
    int function(int arg);
    void function2(char *arg1, char arg2);
    void function3(char *arg1, char **arg2);
    char* function4(int arg1, char *arg2);
    
    typedef enum {
        zero = 0,
        one,
        two
    } basic_enum;
    '''
    ffi.cdef(include)

    ffi.set_source('_test_builder', include+'''
    int function(int arg){
        return arg;
    }
    void function2(char *arg1, char arg2){
        arg1[0] = arg2;
    }
    void function3(char *arg1, char **arg2){
        *arg2 = arg1;
    }
    char* function4(int arg1, char *arg2){
        if (arg1 > 0) {
            return arg2;
        } else {
            return NULL;
        }
    }
    ''')
    return ffi.compile(verbose=True)

mod = compile_test_lib()

# Build the cffi module in a separate process to no taint the local modules space
# p = Process(target=compile_test_lib)
# p.start()
# p.join()

moddir = abspath(dirname(mod))
if moddir not in sys.path:
    sys.path.insert(0, moddir)

# noinspection PyUnresolvedReferences
import _test_builder as _test
lib = cfficloak.cloak(_test)


def test_gen_skeleton():
    tempdir = Path(tempfile.mkdtemp())
    try:
        skel = tempdir / 'skel.py'
        generate_function_skeletons(wrappedlib=lib, skeleton_file=skel)

        skel = import_file(skel)

        assert skel.function(1) == 1

        test = bytearray(b"test")
        assert skel.function2(test, b'z') is None
        assert test == b"zest"

        assert skel.function4(True, b'test') == b'test'
        with pytest.raises(NullError):
            skel.function4(False, b'test')

    finally:
        remove_tree(str(tempdir))


def test_append_skeleton():
    tempdir = Path(tempfile.mkdtemp())
    try:
        skel = tempdir / 'skel.py'
        generate_function_skeletons(wrappedlib=lib, skeleton_file=skel)

        # Edit the generated skeleton
        text = skel.read_text()
        sections = text.split('@cfficloak.function_skeleton')

        # keep just function3 skeleton
        text = '@cfficloak.function_skeleton'.join(
            [sec for sec in sections if "function3" in sec or "import" in sec])

        text = text.replace(
            '@cfficloak.function_skeleton(lib)',
            '@cfficloak.function_skeleton(lib, noret=True, outargs=[1])')
        text = text.replace(
            'def function3(arg0, arg1)',
            'def function3(arg0)')

        with skel.open('w') as handle:
            handle.write(text)

        text = skel.read_text()
        assert 'function4' not in text

        # Regenerate the skeleton - our changes should not be overwritten
        # but the missing function should be re-added
        generate_function_skeletons(wrappedlib=lib, skeleton_file=skel)

        # noinspection PyUnresolvedReferences
        skel = import_file(skel)

        # Check the outarg is made and returned for us
        assert lib.ffi.string(skel.function3(b'test')) == b'test'

        # Check the rest of the functions were re-generated
        assert skel.function4(True, b'test') == b'test'
        assert skel.function(1) == 1
        assert skel.function2  # exists

    finally:
        remove_tree(str(tempdir))

# def test_include_header():
#     cmoka = Path(__file__).parent / 'gumbo-parser-0.10.1'
#     src = cmoka / 'src'
#     header = src / 'gumbo.h'
#
#     source_files = src.glob('*.c')  # type: [Path]
#     source = '\n'.join([f.read_text() for f in source_files])
#
#     pre_declare = '''
#
#     '''
#     parsed = parse_header(content=pre_declare, filename=header, includes=[src])
#     # remove inline defined function
#     # parsed = re.sub(r'static inline void _unit_test_dummy.*}', '', parsed, flags=re.DOTALL)
#
#     ffi = FFI()  # type: cffi.FFI
#     ffi.cdef(parsed, override=True)
#     ffi.set_source('gumbo', source)
#     ffi.compile(verbose=True)
#
#     import gumbo
#     gumbo = cfficloak.cloak(gumbo)
#     print(dir(gumbo))
#
#     # for token in parse_header(filename=header):
#     #     try:
#     #         token)
#     #     except:
#     #         failed.append(token)
#
#     # cdef = cdef.replace('va_list', '...')
