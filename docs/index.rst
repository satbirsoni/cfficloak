.. cfficloak documentation master file, created by
   sphinx-quickstart on Wed Sep 27 11:56:29 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cfficloak documentation!
=====================================

CFFIwrap is a set of convenience functions and wrapper classes designed to
make writing CFFI modules less tedious. 

Simply install cfficloak release from pypi:
   pip install cfficloak  

or development version:
   pip install git+https://gitlab.com/alelec/cfficloak.git

For more examples take a look in the tests directory.

This project is licensed under the Apache License, version 2.0.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   cfficloak


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
