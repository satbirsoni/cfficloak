# Copyright (c) 2016, Andrew Leech <andrew@alelec.net>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt

import os
import sys
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.txt')) as readme:
    long_description = readme.read()

cffi = ['cffi>=1.6']
install_requires=['six', 'setuptools_scm', 'wrapt']
tests_require = cffi + ['setuptools_bin_targets', 'pytest',
                        'pytest-runner', 'pytest-cov', 'numpy']

PY2 = sys.version_info[0] == 2
if PY2:
    install_requires.append('pathlib2')
    tests_require.append('pathlib2')


setup(
    name='cfficloak',
    packages=['cfficloak'],
    description='A simple but flexible module for creating '
                'object-oriented, pythonic CFFI wrappers.',
    long_description=long_description,
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    url='https://gitlab.com/alelec/cfficloak',
    use_scm_version=True,
    include_package_data=True,
    install_requires=install_requires,
    setup_requires=['setuptools_scm'],
    tests_require=tests_require,
    zip_safe= False,
    extras_require={
        'cffi': cffi,
        'test': tests_require,
        'build': ['ply', 'pcpp'] + cffi},
)
